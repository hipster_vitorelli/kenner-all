import "../styles/index.scss";
import $ from "jquery";
import "jquery-mask-plugin";
import Swal from "sweetalert2";
import axios from "axios";
import defaultLocation from "./api";
import Email from "./email";

$(".selo-text-box").click(function() {
  var id = $(this).attr("id");
  $("#" + id + " .selo-text-subtitle").slideToggle();
  $("#" + id + " .fa-chevron-up").toggleClass("ativo");
  $("#" + id + " .fa-chevron-down").toggleClass("ativo");
});

$(".links-box").click(function() {
  var id = $(this).attr("id");
  $(".selo-link").removeClass("ativo");
  $("#" + id + " .selo-link").addClass("ativo");
  $(".selo-text-content").removeClass("ativo");
  $(".selo-text-content#" + id).addClass("ativo");
});

Email();

axios
  .get(window.location.origin + "/no-cache/profileSystem/getProfile")
  .then(function(response) {
    window.cliente = response.data;
    axios
      .get(defaultLocation + "/cliente/buscar?email=" + response.data.Email)
      .then(function(response) {
        const garantias = response.data[0].pedidos;
        if (garantias.length < 1) {
          let html = `
        <div class="sem-garantia">
            <img src="/arquivos/selo-garantias-novo-icon.png" />
            <p>Você ainda não possui nenhuma garantia cadastrada.<br>
            Para realizar uma troca, clique em "Cadastrar Nova"
        </div>
        `;
          $(".selo-garantias").append(html);
        } else {
          let html = `
        <div class="garantias-legenda">
            <p>Legenda:</p>
            <div class="legenda-title">
                <img src="/arquivos/icon-legenda-selo.png" alt="Selo" />
                <p>Com código do selo</p>
            </div>
            <div class="legenda-title">
                <img src="/arquivos/icon-legenda-nota.png" alt="Nota Fiscal" />
                <p>Com nota fiscal</p>
            </div>
        </div>
        `;

          for (let i = 0; i < garantias.length; i++) {
            // LINHA e TAMANHO
            const arrayItens = JSON.parse(garantias[i].pedido.items);
            const qtdItens = arrayItens.length;

            // VALIDADE
            const data = garantias[i].pedido.data_compra;
            function somarAno(data) {
              let dataFinal = data.split("-"),
                dia = dataFinal[2],
                mes = dataFinal[1],
                ano = parseInt(dataFinal[0].slice(2)) + 1,
                anoFinal = dataFinal[0].slice(0, 2) + ano;
              return [
                mes + "/" + dia + "/" + anoFinal,
                dia + "/" + mes + "/" + anoFinal
              ];
            }
            const anoVencimento = somarAno(data);

            //ID DO PEDIDO
            const id_pedido = garantias[i].id;

            // STATUS
            const status = garantias[i].pedido.status_selo;

            // SELO
            const selo = garantias[i].pedido.selo_number;

            // VENCIDO
            function pegarDataAtual() {
              let d = new Date();
              let month = d.getMonth() + 1;
              let day = d.getDate();
              return (
                d.getFullYear() +
                "/" +
                (("" + month).length < 2 ? "0" : "") +
                month +
                "/" +
                (("" + day).length < 2 ? "0" : "") +
                day
              );
            }

            const dataVencimento = new Date(anoVencimento[0]);
            const dataAtual = new Date(pegarDataAtual());

            if (dataVencimento < dataAtual) {
              $(".garantias-conteudo").addClass("vencido");
            }

            html += `
                    ${
                      selo !== "000000000000000"
                        ? `
                        ${
                          dataVencimento < dataAtual
                            ? `<div class="garantias-conteudo icon-selo vencido">`
                            : `<div class="garantias-conteudo icon-selo">`
                        }`
                        : `
                        ${
                          dataVencimento < dataAtual
                            ? `<div class="garantias-conteudo icon-nota vencido">`
                            : `<div class="garantias-conteudo icon-nota">`
                        }`
                    }
                        <div class="conteudo-legenda"></div>
                            <div class="garantia-itens">
                    `;

            for (let j = 0; j < qtdItens; j++) {
              html += `
                  <div class="container-troca">
                    <div class="garantia-item" idpedido="${id_pedido}">
                        <div class="conteudo-linha">
                            <p>Linha:</p>
                            <div id="linha-text" class="text">${
                              arrayItens[j].modelo
                            }</div>
                        </div>
                        <div class="conteudo-tamanho">
                            <p>Tamanho:</p>
                            <div id="tamanho-text" class="text">${
                              arrayItens[j].tamanho
                            }</div>
                        </div>
                        <div class="conteudo-validade">
                            <p>Validade:</p>
                            <div id="validade-text" class="text">${
                              anoVencimento[1]
                            }</div>
                        </div>
                        <div class="conteudo-status">${(() => {
                          if (status == "Pendente") {
                            return `<div id="status-text" style="background:#fcb800" class="pendente">`;
                          } else if (status == "Aprovado") {
                            return `<div id="status-text" class="aprovado">`;
                          } else {
                            return `<div id="status-text" class="analise" style="background: #fa5a01 !important;">`;
                          }
                        })()}${status}</div>
                        </div>
                        <div class="conteudo-trocar">
                          ${(() => {
                            if (status == "Aprovado") {
                              return `<div class="trocar-img ativo"></div>
                              <a src="" class="ativo">Trocar este produto</a>`;
                            } else if (status == "aprovado") {
                              return `<div class="trocar-img ativo"></div>
                              <a src="" class="ativo">Trocar este produto</a>`;
                            } else {
                              return `<div class="trocar-img desativado"></div>
                              <a class="desativado" style="cursor: not-allowed;">Trocar este produto</a>`;
                            }
                          })()}
                        </div>
                    </div>
                    <form id="formulario-${id_pedido}-${Math.floor(
                Math.random() * (99999999 - 1)
              ) + 1}" enctype="multipart/form-data">
              <input type="text" value="${id_pedido}" name="id_troca" class="id_troca" style="display:none"; />
                      <div class="troca-de-intens">
                      <div class="content">
                        <div class="conteudo">
                          <div class="box anexos">
                            <p>Anexar foto do produto</p>
                            <div class="box-button">
                              <div class="anexo">
                                <img src="/arquivos/seta-upload.png?v=637075428556400000">Anexar arquivo
                              </div>
                              <input style="display: none;" type="file" name="fileUploads" multiple></div>
                          </div>
                          <div class="box comentario">
                            <p>Comentário:</p>
                            <textarea name="comentario" placeholder="Explique aqui o motivo da sua troca"></textarea>
                            <div class="alinhamento">
                              <div class="enviar">Enviar</div>
                            </div>  
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
              </div>
                    `;
            }
            html += "</div></div>";
          }
          $(".selo-garantias").append(html);
        }

        //Funcao de clique
        $(".garantia-itens")
          .find(".container-troca .conteudo-trocar a")
          .click(".ativo", function(e) {
            e.preventDefault();
            var $this = $(this);
            if ($this.hasClass("ativo") == true) {
              $this
                .parents(".container-troca")
                .find("form .troca-de-intens")
                .slideToggle();
            }
          });

        //CHANGE ANEXO
        $(".garantia-itens")
          .find(".anexo")
          .click(function(e) {
            e.preventDefault();
            var $this = $(this);
            $this.siblings("input").trigger("click");
          });

        function Disparo(classe, linha, tamanho, validade, selo) {
          $(`${classe}`).each(function() {
            var formulario = document.querySelector(`${classe}`);
            var formData = new FormData(formulario);
            formData.append("linha", linha);
            formData.append("tamanho", tamanho);
            formData.append("validade", validade);
            formData.append("selo", selo);

            $.ajax({
              url: `${defaultLocation}/troca`,
              data: formData,
              cache: false,
              contentType: false,
              processData: false,
              method: "POST",
              timeout: 0,
              mimeType: "multipart/form-data",
              type: "POST",
              success: function(data, textStatus, jqXHR) {
                $(`
                <section class="master confirm-troca-form" style="/* display: none; */"><div class="content"><div class="conteudo"><h1>Solicitação de troca  enviada para análise</h1><p>As fotos do produto foram enviadas para a nossa equipe de qualidade. Em <br> até sete dias você receberá uma resposta por email com o resultado da <br> análise. Caso sua troca seja deferida você receberá um cupom para <br> escolher uma nova Kenner no nosso site.<br><br>
                Em caso de dúvidas, entre em contato através do “Fale conosco”</p><div class="fale-conosco"><a href=""><img src="/arquivos/support 2.png?v=637075428566900000" alt=""> Ir para "Fale conosco"</a></div><div class="ver-garantias"><a href="/Garantias/minhas-garantias">Ver minhas garantias</a></div></div></div></section>
                `).insertAfter($(".selo-content-pai"));
                $(".selo-content-pai").slideUp(200);
                $(".confirm-troca-form").slideDown(400);

                $(".ver-garantias a").click(function(e) {
                  e.preventDefault();

                  $(".selo-content-pai").slideDown(200);
                  $(".confirm-troca-form").slideUp(400);
                });
              }
            });
          });
        }

        //ENVIO DE FORMULARIO
        $(".garantia-itens")
          .find(".enviar")
          .click(function() {
            var initial = $(this).parents(".container-troca");
            var linha = initial.find("#linha-text").text();
            var tamanho = initial.find("#tamanho-text").text();
            var validade = initial.find("#validade-text").text();
            var selo = initial.find("#status-text").text();

            var classe = $(this)
              .parents("form")
              .attr("id");

            Disparo(`#${classe}`, linha, tamanho, validade, selo);
          });

        $(".garantias-conteudo")
          .find("> .garantia-itens")
          .each(function() {
            $(this)
              .find(".container-troca .garantia-item")
              .each(function(e) {
                var $this = $(this);
                var id_pedido = $(this).attr("idpedido");
                $.get(
                  `${defaultLocation}/trocas?id_do_pedido=${id_pedido}`
                ).then(e => {
                  console.log(e);
                  if (e !== null) {
                    var item = e.troca.pedido.linha;

                    if ($this.find("#linha-text").text() == item) {
                      $this
                        .find("div#status-text")
                        .text(e.status)
                        .css("background", "#fcb800");

                      $this
                        .find(".conteudo-trocar .trocar-img")
                        .removeClass("ativo")
                        .addClass("desativado");
                      $this
                        .find(".conteudo-trocar a")
                        .removeClass("ativo")
                        .addClass("desativado")
                        .text("Aguardando análise");
                    }
                  }
                });
              });
          });

        //FIM
      });
  });
