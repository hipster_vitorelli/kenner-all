import $ from "jquery";
import axios from "axios";

export default function Email() {
    axios.get(window.location.origin + '/no-cache/profileSystem/getProfile').then(function (response) {
        const name = response.data.FirstName != null ? response.data.FirstName : response.data.Email.slice(0,10) + "...";
            $("#user-name").text(name + "!");
    });
}
