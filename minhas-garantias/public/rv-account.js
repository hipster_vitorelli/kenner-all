var gt__enderecos = function() {

	$('.address-display-unit').wrap( "<div class='show-content'></div>" );
	var nAd = 1;
	$('.show-content').each(function() {
		var newTitle = $('.street', this).text();
		$(this).prepend('<a class="item open-aba" rel="0'+nAd+'">'+newTitle+'</a>');
		$(this).addClass('b-0'+nAd);
		$('.address-display-unit', this).addClass('aba a-0'+nAd);
		$('h5', this).remove();
    	nAd = nAd+1;
    });

    $('.address-display .show-content:eq(0)').addClass('ativo');
	$('.address-display .aba:eq(0)').addClass('show');
	$('.address-display .show-content:eq(0) a.item').removeClass('open-aba');
	$('.address-display .aba:eq(0)').slideDown();

	$('body').on('click', '.open-aba', function() {

		var thisAba 	= '.a-'+$(this).attr('rel');
		var thisBotao	= '.b-'+$(this).attr('rel');
		$('.address-display .show').slideUp(function(){
			$('.address-display .show-content').removeClass('ativo');
			$(thisBotao).addClass('ativo');
			$('.address-display .show-content a.item').addClass('open-aba');
			$('.address-display .aba').removeClass('show');
			$(thisAba).addClass('show');

			$(thisBotao+' a.item').removeClass('open-aba');
			$(thisAba).slideDown();

		});
	});
}

var gt__insere_nome = function(){
	var name_cliente = $('.profile-detail-display h5').text();
	$('.header-client h3 span').text(name_cliente);
}

/*modal*/
var gt__modal = function(elemButtom) {

	$('body').on('click', elemButtom, function(){
		var open_modal = $(this).attr('href');
		$(open_modal).show();
		$('body').append('<div class="modal-backdrop"></div>');
		$('html, body').animate({scrollTop: 0}, 1000);
		$("#business-toggle").text('Incluir dados de pessoa jurídica');
	});

	$('.modal-header').on('click', '.close', function(){
		$('.modal-backdrop').remove();
		$('.modal').hide('fast');
	});
}

/*order*/
var gt__num_pedidos = function() {
	var totalPedido = $('.order-client .order-title').length;
	$('.header-client h3 span').text(totalPedido);
}


$(document).ready(function() {
	console.log('a1');

	if ($('body.client-account').length) {
		gt__enderecos();
		gt__insere_nome();
		gt__modal('#edit-data-link');
		gt__modal('.address-update');
		gt__modal('.delete');
	}
	if ($('body.client-orders').length > 0) {
		
		// $("link[href='//io.vtex.com.br/front-libs/bootstrap/2.3.2/css/bootstrap.min.css']").remove();
		// $(".box-18.carrinho").remove();
		// $("style:nth-of-type(1)").remove();
		// $("style:nth-of-type(2)").remove();
		$(".ordergroup").each(function(){
			var infoPagamento = $(this).find(".order-info");
			$(infoPagamento).insertAfter($(this).find(".order-details"));
		});
	}

});

$(window).load(function(){
	$('<link type="text/css" rel="stylesheet" media="all" href="//santorock.vteximg.com.br/arquivos/rv-bootstrap.css>').insertAfter("link:nth-of-type(2)");
});

$(document).ajaxStop(function(){
	if ($('body.client-orders').length) {
		gt__num_pedidos();

	}
});