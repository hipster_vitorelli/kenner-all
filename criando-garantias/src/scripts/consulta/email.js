import $ from "jquery";
import axios from "axios";

export default function Email(){
  axios.get(window.location.origin + '/no-cache/profileSystem/getProfile').then(function(response){
    const email = response.data.Email;
    const user = response.data.FirstName;
    $(".cliente h1 strong").text(user);
    window.base_email = email;
    $("input#email").val(window.base_email).attr('disabled', 'disabled').attr("placeholder", `${window.base_email}`).css("background", "#efefef");
  });
}
