import axios from "axios";
import defaultLocation from "../api";
import Swal from "sweetalert2";

class Dados {
  async ConsultaEmail(email) {
    const consulta = await axios.get(
      `${defaultLocation}/cliente/buscar?email=${email}`
    );

    if (consulta.data.length == 0) {
      Swal.fire({
        type: "error",
        title: "Oops...",
        html: "Precisamos atualizar alguns <br /> dados antes de prosseguir",
        animation: true,
        showConfirmButton: false,
        showCloseButton: false,
        timer: 5000,
        customClass: {
          container: "falta-informacao"
        },
        footer:
          '<img src="https://selodegarantia.kenner.com.br/Content/Img/logo_kenner_heather.png" />',
        onAfterClose: () => {
          $("section.master.passo-um-form").hide();
          $("section.master.passo-dados-form").slideDown();
        }
      });

      return false;
    }

    Swal.fire({
      title: "Maravilha!",
      html: "Encontramos seus dados <br /> em nossa base.",
      animation: true,
      showConfirmButton: false,
      showCloseButton: false,
      timer: 4000,
      customClass: {
        container: "falta-informacao"
      },
      footer:
        '<img src="https://selodegarantia.kenner.com.br/Content/Img/logo_kenner_heather.png" />',
      onAfterClose: () => {
        $("section.master.passo-um-form").hide();
        $("section.master.passo-tres-form").slideDown();
      }
    });
  }
}

export default new Dados();
