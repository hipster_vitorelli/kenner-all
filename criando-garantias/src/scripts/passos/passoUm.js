import $ from "jquery";
import Swal from "sweetalert2";
import axios from "axios";
import defaultLocation from "../api";
import Dados from "../consulta/dados";

function semSelo() {
  $(".passo-um-form .cliente .controles .nao").click(function() {
    var $this = $(this);
    $this.text("aguarde...");

    axios
      .get(`${defaultLocation}/selo?selo=000000000000000`)
      .then(function(result) {
        if (result.data.Codigo === undefined) {
          Swal.fire({
            type: "error",
            title: "Oops...",
            html:
              "Encontramos um erro em nossos servidores. Tente novamente em alguns instantes",
            animation: true,
            showConfirmButton: false,
            showCloseButton: false,
            timer: 6000,
            customClass: {
              container: "falta-informacao"
            },
            footer:
              '<img src="https://selodegarantia.kenner.com.br/Content/Img/logo_kenner_heather.png" />'
          });
        } else {
          $this.text("ok");
          Dados.ConsultaEmail(window.base_email);
        }
      })
      .catch(function(error) {
        Swal.fire({
          type: "error",
          title: "Oops...",
          html: "Houve um erro em nossa base. Por favor, tente novamente.",
          animation: true,
          showConfirmButton: false,
          showCloseButton: true,
          timer: 6000,
          customClass: {
            container: "selo-incorreto"
          }
        });
        $this.text("Tentar novamente");
      });
  });
}

function comSelo() {
  $(".passo-um-form .cliente .controles .sim").click(function() {
    var $this = $(this);
    Swal.fire({
      title: "Fique Ligado",
      html: `<ul>
              <li>
                <div class="box bubble">1</div>
                <div class="box">Tenha em mãos a nota fiscal de compra de sua Kenner.
                Caso não possua, entre em contato com o Fale Conosco.</div>
              </li>
              <li>
                <div class="box bubble">2</div>
                <div class="box">Você precisa ter o cartão individual do selo ou ter comprado depois do dia 26/09/18.</div>
              </li>
              <li>
                <div class="box bubble">3</div>
                <div class="box">A data de compra informada na nota deve estar de acordo com o período de 12 meses da garantia. </div>
              </li>
            </ul>
            <p>Ao clicar em “OK” você concorda com os termos do regulamento de cadastro e solicitação do Selo de Garantia.</p>
            <div class="leia-regulamento">
              <div><img src="/arquivos/Group.png" /></div>
              <div><a href="/garantias" target="_blank">Leia o regulamento</a></div>
            </div>
            `,
      animation: true,
      showConfirmButton: true,
      confirmButtonText: "ok",
      confirmButtonClass: "fecha-ok-ligado",
      showCloseButton: true,
      width: 320,
      customClass: {
        container: "fique-ligado"
      }
    }).then(result => {
      if (result.value) {
        buscaSelo();
        $(".passo-um-form").hide();
        $(".passo-dois-form").slideDown();
      }
    });
  });
}

function buscaSelo() {
  $(".passo-dois-form .cliente .controles .ok").click(function() {
    var $this = $(this);
    $this.text("aguarde...");
    axios
      .get(`${defaultLocation}/selo?selo=${$("input#selo_number").val()}`)
      .then(function(result) {
        if (result.data.Codigo === undefined) {
          $this.text("tentar novamente");
          Swal.fire({
            type: "error",
            title: "Oops...",
            html: "O Selo inserido está incorreto ou não consta em nossa base",
            animation: true,
            showConfirmButton: false,
            showCloseButton: true,
            timer: 6000,
            customClass: {
              container: "selo-incorreto"
            }
          });
        } else {
          $this.text("ok");
          $(".passo-dois-form").hide();
          $(".passo-tres-form").slideDown();
        }
      })
      .catch(function(error) {
        Swal.fire({
          type: "error",
          title: "Oops...",
          html: "Houve um erro em nossa base. Por favor, tente novamente.",
          animation: true,
          showConfirmButton: false,
          showCloseButton: true,
          timer: 6000,
          customClass: {
            container: "selo-incorreto"
          }
        });
        $this.text("Tentar novamente");
      });
  });
}

export { semSelo, comSelo, buscaSelo };
