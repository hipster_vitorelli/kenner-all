import "../styles/index.scss";
import $ from "jquery";
import "jquery-mask-plugin";
import Swal from "sweetalert2";
import axios from "axios";
import defaultLocation from "./api";
import Email from "./consulta/email";
import { semSelo, comSelo, buscaSelo } from "./passos/passoUm";
import Pares from "./acoes/pares";

Email();
semSelo();
comSelo();
Pares();

$("input#cnpj").mask("99.999.999/9999-99");
$("input#cpf").mask("999.999.999-99");
$("input#nome").attr("maxlength", 100);
$("input#sobrenome").attr("maxlength", 100);
$("input#ddd_telefone").mask("99");
$("input#ddd_celular").mask("99");
$("input#telefone").mask("0000-0000");
$("input#celular").mask("0000-00009");
$("input#celular").blur(function() {
  if ($(this).val().length == 10) {
    $("input#celular").mask("00000-0009");
  } else {
    $("input#celular").mask("0000-00009");
  }
});
$("input.cep").mask("99999-999");

function ClickUpload() {
  $(".anexo").click(function() {
    $("input#fileUpload").trigger("click");
  });
}
ClickUpload();

function limpa_formulário_cep() {
  // Limpa valores do formulário de cep.
  $("input.endereco").val("");
  $("input.bairro").val("");
  $("input.municipio").val("");
  $("input.estado").val("");
  $("#ibge").val("");
}

//CEP
$("input.cep").blur(function() {
  var cep = $(this)
    .val()
    .replace(/\D/g, "");
  if (cep != "") {
    var validacep = /^[0-9]{8}$/;
    if (validacep.test(cep)) {
      $("input#logradouro").val("...");
      $("input.bairro").val("...");
      $("input.cidade").val("...");
      $("input.estado").val("...");
      $.getJSON(
        "https://viacep.com.br/ws/" + cep + "/json/?callback=?",
        function(dados) {
          if (!("erro" in dados)) {
            $("input#logradouro")
              .val(dados.logradouro)
              .attr("disabled", "disabled");
            $("input.bairro")
              .val(dados.bairro)
              .attr("disabled", "disabled");
            $("input.cidade")
              .val(dados.localidade)
              .attr("disabled", "disabled");
            $("input#estado")
              .val(dados.uf)
              .attr("disabled", "disabled");
          } else {
            limpa_formulário_cep();
            alert("CEP não encontrado.");
          }
        }
      );
    } else {
      limpa_formulário_cep();
      alert("Formato de CEP inválido.");
    }
  } else {
    limpa_formulário_cep();
  }
});

$(".enviar-form").click(function() {
  var $this = $(this);
  $this.text("Aguarde...");

  const pecas = [];
  $(".line.line-pecas")
    .find(".modelo")
    .each(function() {
      var $this = $(this);
      let modelo = $this.find("#linha-kenner").val();
      let tamanho = $this.find("#tamanho-produto").val();
      pecas.push({ modelo: modelo, tamanho: tamanho });
    });

  $("form#form-compra").each(function() {
    var form = $(this);
    var formdata = false;
    if (window.FormData) {
      formdata = new FormData(form[0]);
    }
    formdata.append("selo_number", $("input#selo_number").val());
    formdata.append("items", JSON.stringify(pecas));
    if ($("input#selo_number").val() != "000000000000000") {
      formdata.append("status_selo", "Aprovado");
    } else {
      formdata.append("status_selo", "Pendente");
    }
    $.ajax({
      url: `${defaultLocation}/pedido?email=${window.base_email}`,
      data: formdata ? formdata : form.serialize(),
      cache: false,
      contentType: false,
      processData: false,
      method: "POST",
      timeout: 0,
      mimeType: "multipart/form-data",
      type: "POST",
      success: function(data, textStatus, jqXHR) {
        $this.text("Enviado com sucesso");
        Swal.fire({
          type: "success",
          title: "Pedido enviado com sucesso!",
          animation: true,
          showConfirmButton: false,
          showCloseButton: false,
          timer: 2000,
          onAfterClose: () => {
            if ($("input#selo_number").val() != "000000000000000") {
              $(".passo-tres-form").hide();
              $(".passo-quatro-form").slideDown();
            } else {
              $(".passo-tres-form").hide();
              $(".passo-cinco-form").slideDown();
            }
          }
        });
      },
      error: function(error) {
        Swal.fire({
          type: "error",
          title: "Oops...",
          text:
            "Alguns dados não foram preenchidos corretamente ou estão vazios. Verfique suas informações e tente novamente",
          animation: true,
          showConfirmButton: false,
          showCloseButton: true
        });
        $this.text("Tentar novamente");
        return false;
      }
    });
  });
});

$(".cadastrar-novo-usuario").click(function() {
  const cliente = {
    nome: $("input#nome").val(),
    sobrenome: $("input#sobrenome").val(),
    email: $("input#email").val(),
    cpf: $("input#cpf").val(),
    ddd_telefone: $("input#ddd_telefone").val(),
    telefone: $("input#telefone").val(),
    ddd_celular: $("input#ddd_celular").val(),
    celular: $("input#celular").val(),
    nascimento: new Date($("input#data_nascimento").val()),
    cep: $("input.cep").val(),
    logradouro: $("input#logradouro").val(),
    numero: $("input#numero").val(),
    bairro: $("input.bairro").val(),
    complemento: $("input#complemento").val(),
    cidade: $("input.cidade").val(),
    estado: $("input#estado").val(),
    pedidos: []
  };

  const $this = $(this);
  $this.text("aguarde...");

  if ($("input#email").val() == base_email) {
    axios
      .post(`${defaultLocation}/cliente/atualizar`, cliente)
      .then(function(response) {
        console.log(response);
        Swal.fire({
          type: "success",
          title: "Cadastro atualizado com sucesso!",
          animation: true,
          showConfirmButton: false,
          showCloseButton: false,
          timer: 2000,
          onAfterClose: () => {
            $(".passo-um-form").hide();
            $(".passo-dois-form").hide();
            $(".passo-tres-form").slideDown();
            $(".passo-quatro-form").hide();
            $(".passo-dados-form").hide();
            sessionStorage.setItem("id", response.data._id);
            sessionStorage.setItem("email", $("input#email").val());
          }
        });
      })
      .catch(function(error, response) {
        Swal.fire({
          type: "error",
          title: "Oops...",
          html: "Já existe um cliente com este email cadastrado!",
          animation: true,
          showConfirmButton: false,
          showCloseButton: false,
          timer: 5000,
          customClass: {
            container: "falta-informacao"
          },
          onAfterClose: () => {
            window.location.href = "/garantias/";
          },
          footer: '<img src="/arquivos/logo_kenner_heather.png" />'
        });
        $this.text("Tentar novamente");
      });
  } else {
    axios
      .post(`${defaultLocation}/cliente`, cliente)
      .then(function(response) {
        console.log(response);
        Swal.fire({
          type: "success",
          title: "Cadastro inicial realizado com sucesso!",
          animation: true,
          showConfirmButton: false,
          showCloseButton: false,
          timer: 2000,
          onAfterClose: () => {
            $(".passo-um-form").hide();
            $(".passo-dois-form").hide();
            $(".passo-tres-form").slideDown();
            $(".passo-quatro-form").hide();
            $(".passo-dados-form").hide();
            sessionStorage.setItem("id", response.data.cliente._id);
            $(".passo-cinco-form").hide();
          }
        });
      })
      .catch(function(error, response) {
        Swal.fire({
          type: "error",
          title: "Oops...",
          html: "Já existe um cliente com este email cadastrado!",
          animation: true,
          showConfirmButton: false,
          showCloseButton: false,
          timer: 5000,
          customClass: {
            container: "falta-informacao"
          },
          onAfterClose: () => {
            window.location.href = "/garantias/";
          },
          footer: '<img src="/arquivos/logo_kenner_heather.png" />'
        });
        $this.text("Tentar novamente");
      });
  }
});
