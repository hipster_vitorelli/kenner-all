import $ from "jquery";
import axios from "axios";
import defaultLocation from "../api";

function changeLinha(){
  $("select#linha-kenner").change(function(){
    const $this = $(this);
    const valor = $(this).find("option:selected").attr('param');
    axios.get(`${defaultLocation}/produto/tipo/tamanhos?id=${valor}`).then(function(obj){
      const mapa = $this.parents('.linha').find("#tamanho-produto");
      obj.data.map(req =>
        $(mapa).append(
          `<option value="${req.Descricao}">${req.Descricao}</option>`
        )
      );
    });
  });
}

function findAllLinha(){
  axios.get(`${defaultLocation}/produto`).then(function(obj){
    obj.data.map(req =>
      $("select#linha-kenner").append(
        `<option param="${req.Id}" value="${req.Descricao}">${req.Descricao}</option>`
      )
    );
  }).catch(function(){
    findAllLinha();
  });
}

export default function Pares(){
  $("input#num_de_pares").change(function(){
    const pares = $(this).val();
    if(pares <= 0){
      $(".line-pecas").empty();
      alert("Selecione a quantidade");
    }else{
      $(".line-pecas").empty();
      for(var i = 0; i < pares; i++){
        $(".line-pecas").append(`
            <div class="modelo" posicao="${i + 1}">
              <span>Modelo ${i + 1}</span>
              <div class="linha">
                  <div class="box">
                      <label>
                          <span>Linha Kenner:</span>
                      </label>
                      <select name="sandalha-linha-${i + 1}" id="linha-kenner">
                          <option value="selecione">selecione</option>
                      </select>
                  </div>
                  <div class="box">
                      <label>
                          <span>Tamanho:</span>
                      </label>
                      <select name="tamanho-sandalha-${i + 1}" id="tamanho-produto">
                          <option value="selecione">selecione</option>
                      </select>
                  </div>
              </div>
          </div>
        `);
      }
      findAllLinha();
      setTimeout(function(){
        changeLinha();
      },1500);
    }
  });
}