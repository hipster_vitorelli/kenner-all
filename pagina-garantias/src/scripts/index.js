import "../styles/index.scss";
import $ from "jquery";
import "jquery-mask-plugin";
import Swal from "sweetalert2";
import axios from "axios";
import defaultLocation from "./api";
import Email from "./email";

$('.selo-text-box').click(function () {
    var id = $(this).attr('id');
    $('#' + id + ' .selo-text-subtitle').slideToggle();
    $('#' + id + ' .fa-chevron-up').toggleClass('ativo');
    $('#' + id + ' .fa-chevron-down').toggleClass('ativo');
})

$('.links-box').click(function () {
    var id = $(this).attr('id');
    $('.selo-link').removeClass('ativo');
    $('#' + id + ' .selo-link').addClass('ativo');
    $('.selo-text-content').removeClass('ativo');
    $('.selo-text-content#' + id).addClass('ativo');
})

Email();

axios.get(window.location.origin + '/no-cache/profileSystem/getProfile').then(function (response) {

    axios.get(defaultLocation + '/cliente/buscar?email=' + response.data.Email).then(function (response) {

        const garantias = response.data[0].pedidos;

        if (garantias.length < 1) {
            let html = `
        <div class="sem-garantia">
            <img src="/arquivos/selo-garantias-novo-icon.png" />
            <p>Você ainda não possui nenhuma garantia cadastrada.<br>
            Para realizar uma troca, clique em "Cadastrar Nova"
        </div>
        `;
            $('.selo-garantias').append(html);
        } else {

            let html = `
        <div class="garantias-legenda">
            <p>Legenda:</p>
            <div class="legenda-title">
                <img src="/arquivos/icon-legenda-selo.png" alt="Selo" />
                <p>Com código do selo</p>
            </div>
            <div class="legenda-title">
                <img src="/arquivos/icon-legenda-nota.png" alt="Nota Fiscal" />
                <p>Com nota fiscal</p>
            </div>
        </div>
        `;

            for (let i = 0; i < garantias.length; i++) {

                // LINHA e TAMANHO
                const arrayItens = JSON.parse(garantias[i].pedido.items);
                const qtdItens = arrayItens.length;

                // VALIDADE
                const data = garantias[i].pedido.data_compra;
                function somarAno(data) {
                    let dataFinal = data.split("-"),
                        dia = dataFinal[2],
                        mes = dataFinal[1],
                        ano = parseInt(dataFinal[0].slice(2)) + 1,
                        anoFinal = dataFinal[0].slice(0, 2) + ano;
                    return [mes + "/" + dia + "/" + anoFinal, dia + "/" + mes + "/" + anoFinal];
                }
                const anoVencimento = somarAno(data)


                // STATUS
                const status = garantias[i].pedido.status_selo;

                // SELO
                const selo = garantias[i].pedido.selo_number;

                // VENCIDO
                function pegarDataAtual() {
                    let d = new Date();
                    let month = d.getMonth() + 1;
                    let day = d.getDate();
                    return d.getFullYear() + '/' +
                        (('' + month).length < 2 ? '0' : '') + month + '/' +
                        (('' + day).length < 2 ? '0' : '') + day;
                };

                const dataVencimento = new Date(anoVencimento[0]);
                const dataAtual = new Date(pegarDataAtual());

                if (dataVencimento < dataAtual) {
                    $('.garantias-conteudo').addClass('vencido');
                }

                html += `
                    ${selo !== "000000000000000" ? `
                        ${dataVencimento < dataAtual ? `<div class="garantias-conteudo icon-selo vencido">` : `<div class="garantias-conteudo icon-selo">`}` : `
                        ${dataVencimento < dataAtual ? `<div class="garantias-conteudo icon-nota vencido">` : `<div class="garantias-conteudo icon-nota">`}`}
                        <div class="conteudo-legenda"></div>
                            <div class="garantia-itens">
                    `;

                for (let j = 0; j < qtdItens; j++) {
                    html += `
                    <div class="garantia-item">
                        <div class="conteudo-linha">
                            <p>Linha:</p>
                            <div id="linha-text" class="text">
                                ${arrayItens[j].modelo}
                            </div>
                        </div>
                        <div class="conteudo-tamanho">
                            <p>Tamanho:</p>
                            <div id="tamanho-text" class="text">
                            ${arrayItens[j].tamanho}
                            </div>
                        </div>
                        <div class="conteudo-validade">
                            <p>Validade:</p>
                            <div id="validade-text" class="text">
                            ${anoVencimento[1]}
                            </div>
                        </div>
                        <div class="conteudo-status">
                            ${status == "Pendente" ? `<div id="status-text" class="analise">` : `<div id="status-text" class="aprovado">`}
                            ${status}
                            </div>
                        </div>
                        <div class="conteudo-trocar">
                            <div class="trocar-img ativo"></div>
                            <a src="" class="ativo">Trocar este produto</a>
                        </div>
                    </div>
                    `;
                }
                html += "</div></div>";
            }
            $('.selo-garantias').append(html);
        }
    });
});